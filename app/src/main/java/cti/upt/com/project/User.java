package cti.upt.com.project;

import android.net.Uri;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    private String uid;
    private double latitude, longitude;
    private String name;
    private String email;
    private String photoUrl;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    User(String uid, double latitude, double longitude, String name, String email, Uri photoUrl) {
        this.uid = uid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.email = email;
        if(photoUrl!=null)
            this.photoUrl = photoUrl.toString();
        else this.photoUrl=null;
    }

    String getUid() {
        return uid;
    }

    double getLongitude() {
        return longitude;
    }

    double getLatitude() {
        return latitude;
    }

    public String getName() {
        return name;
    }

    String getEmail() {
        return email;
    }

    String getPhotoUrl() {
        return photoUrl;
    }

}
