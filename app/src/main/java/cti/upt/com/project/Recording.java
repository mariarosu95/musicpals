package cti.upt.com.project;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Recording {
    private String uid;
    private String name;
    private String downloadUrl;
    private String instrument;

    public Recording() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Recording.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Recording other = (Recording) obj;
        return (this.downloadUrl == null) ? other.downloadUrl == null : this.downloadUrl.equals( other.downloadUrl );
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.downloadUrl != null ? this.downloadUrl.hashCode() : 0);
        return hash;
    }

    Recording(String uid, String email, String downloadUrl, String instrument) {
        this.uid = uid;
        this.name = email;
        this.downloadUrl = downloadUrl;
        this.instrument = instrument;
    }

    String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    String getDownloadUrl() {
        return downloadUrl;
    }

    String getInstrument() {
        return instrument;
    }
}
